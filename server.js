var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
var waterrower = require("./Waterrower");

app.listen(8000);

function handler(req, res) {

    fs.readFile(__dirname + '/index.html', function (err, data) {
        if (err) {
            res.writeHead(500);
            return res.end('Error loading index.html');
        }

        res.writeHead(200);
        res.end(data);
    });

}

io.on('connection', function (socket) {
    
    setInterval(function () {
        console.log();
        console.log("Stroke Rate ....." + waterrower.readStrokeCount());  // [ - ]
        console.log("Total Speed ....." + waterrower.readTotalSpeed());   // [cm/s]
        console.log("Average Speed ..." + waterrower.readAverageSpeed()); // [cm/s]
        console.log("Distance... ....." + waterrower.readDistance());     // [ m ]
        console.log("Heart Rate ......" + waterrower.readHeartRate());    // [ bpm ]
        
        
        socket.emit('rowerdata', {
            distance: waterrower.readDistance()
        });
        
    }, 50);
});